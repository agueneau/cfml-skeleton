Set Implicit Arguments.
(* Load the CFML library. *)
Require Import CFML.CFLib.
(* Import CF specifications for Pervasives *)
Require Import CFML.Stdlib.Pervasives_proof.
(* Load the examples CF definitions. *)
Require Import Examples_ml.

Lemma my_succ_spec : forall n,
  app my_succ [n]
    PRE (\[])
    POST (fun m => \[ m = n + 1 ]).
Proof.
  intros n. xcf. xret. hsimpl. auto.
Qed.

Lemma my_incr_spec : forall r n,
  app my_incr [r]
    PRE (r ~~> n)
    POST (fun (tt:unit) => r ~~> (n + 1)).
Proof.
  intros r n. xcf.
  xapps. xapps.
Qed.

(* For a tutorial and more examples, see examples/Tuto in the CFML
   repository... *)