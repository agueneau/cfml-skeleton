let my_succ (n: int) =
  n + 1

let my_incr (r: int ref) =
  r := !r + 1
